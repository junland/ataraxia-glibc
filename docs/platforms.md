## Supported platforms
Ataraxia Linux has been ported to 8 different CPU architectures:
```
 * x86_64       - for 64-bit x86 CPUs
 * aarch64      - for 64-bit ARM CPUs
 * armv7l       - for 32-bit ARM CPUs beginning at ARMv7-a (hard-float)
 * ppc64le      - for 64-bit PowerPC CPUs (little-endian)
 * ppc64        - for 64-bit PowerPC CPUs (big-endian)
 * ppc          - for 32-bit PowerPC CPUs (big-endian)
 * riscv64      - for 64-bit RISC V CPUs
 * riscv32      - for 32-bit RISC V CPUs
```
