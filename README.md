# Welcome to Ataraxia Linux (Fork)

## Introduction
Forked from [Ataraxia Linux](https://github.com/ataraxialinux/ataraxia), this varient will attempt to replace the orginal `musl` library with `glibc` as the default libc library.

## Documentation
* [About the build system](docs/aboutbuildsystem.md)
* [Bootstrapping](docs/bootstrapping.md)
* [Supported platforms](docs/platforms.md)

See the ./docs/ folder.
