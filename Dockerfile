#FROM archlinux/base:latest
FROM fedora:latest

ARG dARCH=x86_64

WORKDIR /root

ENV DEBIAN_FRONTEND=noninteractive

#RUN pacman -Syu --noconfirm

#RUN pacman -S --noconfirm base-devel xorriso mtools git pigz python python2 pixz wget curl zlib xz libarchive libtool perl bc autoconf-archive automake autoconf

RUN dnf update -y

RUN dnf install lzma-sdk-devel expat-devel libzstd-devel lz4-devel bzip2-devel libacl-devel libattr-devel acl attr which libarchive-devel libarchive bsdtar autoconf automake git autoconf automake gawk m4 bison flex texinfo patchutils gcc gcc-c++ libtool gettext-devel xorriso glibc-static perl python3 python2 xz-devel mtools pigz gmp-devel mpfr-devel libmpc-devel openssl-devel elfutils-devel zlib-devel xz-devel wget -y

RUN git clone https://github.com/project-gemstone/pkgutils.git && cd pkgutils && make -f Makefile.dynamic && make install

RUN git clone https://gitlab.com/junland/ataraxia-glibc

WORKDIR /root/ataraxia-glibc

ENV BARCH=$dARCH
ENV MKJOBS=2

ENTRYPOINT ["./build", "stage", "1"]